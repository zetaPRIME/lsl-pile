// [sd] Oculux BETA v0.2.0wip
// https://gitlab.com/zetaPRIME/lsl-pile/-/blob/main/Oculux/Oculux%20Core.lsl

/* Copyright (c) 2016-2024 stellarium designs / Syliph (kiri.mistwalker)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// /-28589oculux|both||command|param|param||command|param|param...
// faces: sclera, lens, pupil, iris

integer eyeSide = 1; // left=1 right=-1 (z rotation sign to point out to the side)
integer cmdSide = 0; // which eye(s) the current command is for

integer isDirty = 0;
UpdateTextureAttributes(integer force) {
    if (isDirty + force == 0) return;
    
    float irisSize = ldFloat("irisSize");
    float pupilSize = ldFloat("pupilSize");
    float pupilWidth = ldFloat("pupilWidth");
    
    // 1.01 at 1.2, 1.425 at 0.2, therefore WolframAlpha: log fit {0.2,1.425},{1.2,1.01}
    float tIrisSize = -0.231616*llLog(0.0106415*irisSize);
    float tScleraStretch = 1.16 + irisSize * 0.16; // 1.16 - 1.32;
    float tPupilSize = 1.0/(pupilSize * 4);
    tPupilSize /= irisSize * 0.5 + 0.5;
    if (tPupilSize > 1.75) tPupilSize = 1.75; // prevent texture repeat under normal conditions
    float tPupilWidth = tPupilSize / pupilWidth;
    if (tPupilWidth > 1.75) tPupilWidth = 1.75; // same
    
    integer ors = 1; if (ldInt("scleraSymmetry")) ors = eyeSide;
    integer orp = 1; if (ldInt("txPupilSymmetry")) orp = eyeSide;
    integer ori = 1; if (ldInt("irisSymmetry")) ori = eyeSide;
    
    llSetLinkPrimitiveParamsFast(LINK_THIS, [
        // set up alpha modes
        PRIM_ALPHA_MODE, ALL_SIDES, PRIM_ALPHA_MODE_BLEND, 0,
        PRIM_ALPHA_MODE, 0, PRIM_ALPHA_MODE_NONE, 0,
        PRIM_ALPHA_MODE, 3, PRIM_ALPHA_MODE_EMISSIVE * ldInt("irisEmissive"), 0,
        // shine
        //PRIM_SPECULAR, ALL_SIDES, TEXTURE_BLANK, <1,1,1>, <0,0,0>, 0, <0,0,0>, 0, 0, // only shine on sclera blend ("lens")
        PRIM_SPECULAR, /*1*/ALL_SIDES, TEXTURE_BLANK, <1,1,1>, <0,0,0>, 0, ldVec("shineColor"), ldInt("shineGloss"), 0,
        
        // textures
        PRIM_TEXTURE, 0, ldStr("txSclera"), <ors, tScleraStretch, 0>, <0,0.5,0>, 0,
        PRIM_TEXTURE, 1, ldStr("txSclera"), <ors, tScleraStretch, 0>, <0,0.5,0>, 0,
        PRIM_TEXTURE, 2, ldStr("txPupil"), <tPupilWidth * ors, tPupilSize, 0>, <0,0,0>, ldFloat("pupilRot") * orp * DEG_TO_RAD,
        PRIM_TEXTURE, 3, ldStr("txIris"), <ori,1,0> * tIrisSize, <0,0,0>, ldFloat("irisRot") * ori * DEG_TO_RAD,
        // color and glow
        PRIM_COLOR, 0, ldVec("scleraColor"), 1,
        PRIM_COLOR, 1, ldVec("scleraColor"), 1,
        PRIM_COLOR, 2, ldVec("pupilColor"), ldFloat("pupilAlpha"),
        PRIM_COLOR, 3, ldVec("irisColor"), 1,
        PRIM_FULLBRIGHT, 0, ldInt("scleraEmissive"),
        PRIM_FULLBRIGHT, 1, ldInt("scleraEmissive"),
        PRIM_FULLBRIGHT, 2, ldInt("pupilEmissive"),
        PRIM_GLOW, 3, ldFloat("irisGlow")
    ]);
    
    string animSel = ldStr("animSelect");
    vector animState = ldVec("animState");
    if (animSel == "pupil") {
        UpdateAnimState(3, <0, 0, 0>, ori);
        UpdateAnimState(2, animState, orp);
    } else if (animSel == "iris") {
        UpdateAnimState(2, <0, 0, 0>, orp);
        UpdateAnimState(3, animState, ori);
    } else {
        // reset animations for both faces
        UpdateAnimState(2, <0, 0, 0>, orp);
        UpdateAnimState(3, <0, 0, 0>, ori);
    }
    
    isDirty = 0;
}

UpdateAnimState(integer face, vector st, float sym) {
    if ((integer)(st.z) & ROTATE) {
        llSetTextureAnim((integer)(st.z), face, 1,1,0, TWO_PI, st.x*sym*TWO_PI);
    } else { // scrolling
        llSetTextureAnim((integer)(st.z), face, 1,1,1, 1, st.x*sym);
    }
}

key mSender = NULL_KEY;
ProcessCommandQueue(list cmds) {
    integer i = 0; integer ii = llGetListLength(cmds);
    for (i = i; i < ii; i++) { // 1 to skip the command header
        ProcessCommand(llParseString2List(llList2String(cmds, i), ["|"], []));
    }
    UpdateTextureAttributes(0);
}

ProcessCommand(list par) {
    string cmd = llList2String(par, 0);
    integer locked = ldInt("locked"); // load lock state from persistent storage
    
    if (0) { } // ocd <.<
    
    else if (cmd == "both") { cmdSide = 0; }
    else if (cmd == "left") { cmdSide = 1; }
    else if (cmd == "right") { cmdSide = -1; }
    else if (cmdSide != 0 && cmdSide != eyeSide) { } // block on commands not for this eye
    // lock/unlock
    else if (cmd == "lock") { svInt("locked", 1); }
    else if (cmd == "unlock") { svInt("locked", 0); }
    else if (cmd == "reset") { llResetScript(); }
    else if (cmd == "update" && mSender != NULL_KEY) {
        llSetRemoteScriptAccessPin((integer)llList2String(par,1));
        llRegionSayTo(mSender, -5730, "install.confirm|Oculux Core");
    }
    else if (locked) { } // block on configuration/geometry-altering commands
    // texture appliers (yeah, "insecure", but look at Kemono or Snaggletooth or Regalia or your cache files)
    else if (cmd == "texture.sclera") { svStr("txSclera", llList2String(par, 1)); svInt("scleraSymmetry", 1); svInt("scleraEmissive", 0); isDirty = 1; }
    else if (cmd == "texture.pupil") { svStr("txPupil", llList2String(par, 1)); svInt("pupilSymmetry", 1); svInt("pupilEmissive", 0); isDirty = 1; }
    else if (cmd == "texture.iris") { svStr("texIris", llList2String(par, 1)); svInt("irisSymmetry", 1); svInt("irisEmissive", 0); isDirty = 1; }
    // symmetry settings
    else if (cmd == "sclera.symmetry") { svInt("scleraSymmetry", TrueFalse(llList2String(par, 1))); isDirty = 1; }
    else if (cmd == "pupil.symmetry") { svInt("pupilSymmetry", TrueFalse(llList2String(par, 1))); isDirty = 1; }
    else if (cmd == "iris.symmetry") { svInt("irisSymmetry", TrueFalse(llList2String(par, 1))); isDirty = 1; }
    else if (cmd == "symmetry") { // might as well
        integer s = TrueFalse(llList2String(par, 1));
        svInt("scleraSymmetry", s);
        svInt("pupilSymmetry", s);
        svInt("irisSymmetry", s);
        isDirty = 1;
    }
    // rotation
    else if (cmd == "pupil.rotation") { svFloat("pupilRot", llList2Float(par, 1)); isDirty = 1; }
    else if (cmd == "iris.rotation") { svFloat("irisRot", llList2Float(par, 1)); isDirty = 1; }
    // colors
    else if (cmd == "sclera.color") { svVec("scleraColor", GetColor(llList2String(par, 1))); isDirty = 1; }
    else if (cmd == "pupil.color") { svVec("pupilColor", GetColor(llList2String(par, 1))); isDirty = 1; }
    else if (cmd == "pupil.alpha") { svFloat("pupilAlpha", llList2Float(par, 1)); isDirty = 1; }
    else if (cmd == "iris.color") { svVec("irisColor", GetColor(llList2String(par, 1))); isDirty = 1; }
    // glow/emission config
    else if (cmd == "sclera.emissive") { svInt("scleraEmissive", TrueFalse(llList2String(par, 1))); isDirty = 1; }
    else if (cmd == "pupil.emissive") { svInt("pupilEmissive", TrueFalse(llList2String(par, 1))); isDirty = 1; }
    else if (cmd == "iris.glow") { svFloat("irisGlow", llList2Float(par, 1)); isDirty = 1; }
    else if (cmd == "iris.emissive") { svInt("irisEmissive", TrueFalse(llList2String(par, 1))); isDirty = 1; }
    // part sizes
    else if (cmd == "iris.size") { svFloat("irisSize", llList2Float(par, 1)); isDirty = 1; }
    else if (cmd == "pupil.size") { svFloat("pupilSize", llList2Float(par, 1)); svFloat("pupilWidth", 1); isDirty = 1; } // resets width, reapply on same message if you want it
    else if (cmd == "pupil.width") { svFloat("pupilWidth", llList2Float(par, 1)); isDirty = 1; }
    // shine
    else if (cmd == "shine") { svInt("shineGloss", llList2Integer(par, 1)); svVec("shineColor", GetColor(llList2String(par, 2))); isDirty = 1; }
    // animation
    else if (cmd == "anim") {
        string sel = llToLower(llList2String(par, 1));
        if (sel != "pupil" && sel != "iris") sel = "none"; // clear state
        string type = llToLower(llList2String(par, 2));
        vector st;
        if (type == "none") st = <0,0,0>;
        else if (type == "scroll") {
            st = <llList2Float(par, 3), 0, (ANIM_ON | SMOOTH | LOOP)>;
        }
        else if (type == "rot" || type == "rotate") {
            st = <llList2Float(par, 3), 0, (ANIM_ON | SMOOTH | LOOP | ROTATE)>;
        }
        svStr("animSelect", sel);
        svVec("animState", st);
        isDirty = 1;
    }
    // physical attributes
    else if (cmd == "scale") {
        float s = llList2Float(par, 1);
        llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_SIZE, <s,s,s>]);
    }
    else if (cmd == "angle") {
        llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_ROT_LOCAL, llEuler2Rot(<0, 0, llList2Float(par, 1) * eyeSide> * DEG_TO_RAD)]);
    }
    // and utility
    else if (cmd == "HARDRESET") { llLinksetDataReset(); llResetScript(); } // delete ALL persistent data and reset to stock
}

// utility functions
float Lerp(float start, float end, float proportion) { return start * (1.0 - proportion) + end * proportion; }
integer TrueFalse(string in) { in = llToLower(in); if (in == "1" || in == "true" || in == "yes") return 1; return 0; }
vector GetColor(string in) {
    in = llStringTrim(in, STRING_TRIM);
    string fc = llGetSubString(in, 0, 0);
    if (fc == "<") return (vector)in;
    else if (fc == "#") {
        in = llGetSubString(in, 1, -1);
        if (llStringLength(in) == 6) in += "FF";
        integer hex = (integer)("0x"+in);
        return <(hex >> 24) & 0xFF, (hex >> 16) & 0xFF, ((hex >> 8) & 0xFF)> / 255.0;
    }
    
    float v = (float)in;
    if (v > 0.0 && v <= 1.0) return <1, 1, 1> * v;
    return <1,1,1>; // default to white if failed
}

// load/save
string ldStr(string k) { return llLinksetDataRead("ocx."+k); }
svStr(string k, string v) { llLinksetDataWrite("ocx."+k, v); }
vector ldVec(string k) { return (vector)ldStr(k); }
svVec(string k, vector v) { svStr(k, (string)v); }
integer ldInt(string k) { return (integer)ldStr(k); }
svInt(string k, integer v) { svStr(k, (string)v); }
float ldFloat(string k) { return (float)ldStr(k); }
svFloat(string k, float v) { svStr(k, (string)v); }

setDefault(string k, string v) {
    string sv = ldStr(k);
    if (sv == "") svStr(k, v);
}

default {
    state_entry() {
        llSetTextureAnim(FALSE, ALL_SIDES, 0, 0, 0.0, 0.0, 1.0); // clean up animation artifacts
        
        { // set some sane defaults
            setDefault("txSclera", "6ba38f01-bafa-b4bc-8d03-9dca6f1a7240");
            setDefault("txPupil", "b631e596-4996-67b8-784f-de533d27724b");
            setDefault("txIris", "b8128741-121b-e47e-5e98-766c0535e63b");
            setDefault("scleraSymmetry", "1");
            setDefault("pupilSymmetry", "1");
            setDefault("irisSymmetry", "1");
            
            // no preset tint
            string nt = (string)<1.0, 1.0, 1.0>;
            setDefault("scleraColor", nt);
            setDefault("pupilColor", nt);
            setDefault("irisColor", nt);
            setDefault("pupilAlpha", "1.0"); // visible pls
            
            setDefault("irisGlow", "0.1"); // not too much, but enough to enhance color visibility
            
            setDefault("irisSize", "1.1");
            setDefault("pupilSize", "0.44");
            setDefault("pupilWidth", "1.0"); // 1:1 aspect ratio
            
            // and default material shine
            setDefault("shineGloss", "192");
            setDefault("shineColor", "<0.1,0.1,0.1>");
        } UpdateTextureAttributes(1); // and refresh textures and such
        
        // set up variables from description
        list desc = llGetLinkPrimitiveParams(LINK_THIS, [PRIM_DESC, PRIM_LINK_TARGET, 0, PRIM_DESC]);
        if (llSubStringIndex(llList2String(desc, 0), "right") != -1) eyeSide = -1; // this prim's description for side
        if (llSubStringIndex(llList2String(desc, 1), "demo") != -1) return; // root prim desc for demo status (skip listen)
        
        // 028589 being "0culux" in T9
        llListen(28589, "", llGetOwner(), "");
        llListen(-28589, "", "", "");
    }
    
    changed(integer type) {
        if (type == CHANGED_OWNER) llResetScript();
    }
    
    listen(integer channel, string name, key id, string msg) {
        if (llGetOwnerKey(id) != llGetOwner()) return;
        
        list cmds = llParseString2List(llStringTrim(msg, STRING_TRIM), ["||"], []);
        { // encapsulate for scope; only need these variables for a short time, let GC toss them after
            list hs = llParseString2List(llList2String(cmds, 0), ["|"], []);
            if (llList2String(hs, 0) != "oculux") return;
            string side = llList2String(hs, 1);
            if (side == "left") cmdSide = 1;
            else if (side == "right") cmdSide = -1;
            else cmdSide = 0;
        }
        
        mSender = id;
        ProcessCommandQueue(cmds);
    }
    
    link_message(integer sender, integer num, string msg, key id) {
        if (num != 28589) return;
        // implement here too, to enable demo units
        list cmds = llParseString2List(llStringTrim(msg, STRING_TRIM), ["||"], []);
        { // encapsulate for scope; only need these variables for a short time, let GC toss them after
            list hs = llParseString2List(llList2String(cmds, 0), ["|"], []);
            if (llList2String(hs, 0) != "oculux") return;
            string side = llList2String(hs, 1);
            if (side == "left") cmdSide = 1;
            else if (side == "right") cmdSide = -1;
            else cmdSide = 0;
        }
        
        mSender = NULL_KEY;
        ProcessCommandQueue(cmds);
    }
}
