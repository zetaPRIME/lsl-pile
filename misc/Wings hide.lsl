float glow = 0.02;
integer visible = TRUE;
integer visiblePref = TRUE;
setVisible(integer v) {
    v = !(!v); // booleanize
    if (v != visible) {
        if (v) llPlaySound("Wings on", 1.0);
        else llPlaySound("Wings off", 1.0);
    }
    visible = v;
    llSetLinkPrimitiveParamsFast(LINK_ALL_OTHERS, [ PRIM_COLOR, ALL_SIDES, <1,1,1>, v, PRIM_GLOW, ALL_SIDES, glow*v ]);
}
setVisiblePref(integer v) {
    visiblePref = v;
    if (llGetAgentInfo(llGetOwner()) & AGENT_FLYING) v = TRUE;
    setVisible(v);
}

default {
    state_entry() {
        llListen(777, "", NULL_KEY, "");
        llSetTimerEvent(0.25);
    }
    on_rez(integer params) { setVisible(visible); }
    listen(integer ch, string name, key id, string msg) {
        if (llGetOwnerKey(id) != llGetOwner()) return;
        
        if (msg == "wings on") setVisiblePref(TRUE);
        else if (msg == "wings off") setVisiblePref(FALSE);
        else if (msg == "wings toggle") setVisiblePref(!visiblePref);
    }
    timer() {
        if (!visiblePref) {
            integer flying = !(!(llGetAgentInfo(llGetOwner()) & AGENT_FLYING));
            if (visible != flying) setVisible(flying);
        }
    }
}
