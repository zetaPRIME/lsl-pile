// [sd] Quick Applier for the [BB//MM] Ribbed Corset for Kemono

// - Paste into a new script (if getting this off GitLab)
// - Right click>Edit a copy of your corset
// - Switch the edit floater to the "contents" tab and drag this script in
// All set!

// "Demoness Pink" preset
key txDiffuse = "2ad33208-ee56-5909-76ae-34dcb4d5e051";
key txNormal = "b6834d00-1bb7-eaaa-d82c-16e0d581491d";
key txSpecular = "cc11676c-a53e-0d5a-53dd-f821b614c92f";
string mainColor = "#ffffff";
string shineColor = "#ce7ea6";
integer mainGloss = 80;
integer mainEnv = 0;

string lacingColor = "#0d0d0d";
string lacingShineColor = "#808080";
integer lacingGloss = mainGloss;
integer lacingEnv = mainEnv;

string grommetColor = "#404040";
string grommetShineColor = "#ffffff";
integer grommetGloss = 95;
integer grommetEnv = 5;

integer alphaMode = 2; // mask

/////////////////
// ACTUAL CODE //
/////////////////

vector texReps = <1, 1, 0>;
vector texOffset = <0, 0, 0>;
float texRot = 0;

vector GetColor(string in) {
    in = llStringTrim(in, STRING_TRIM);
    string fc = llGetSubString(in, 0, 0);
    if (fc == "<") return (vector)in;
    else if (fc == "#") {
        in = llGetSubString(in, 1, -1);
        if (llStringLength(in) == 6) in += "FF";
        integer hex = (integer)("0x"+in);
        return <(hex >> 24) & 0xFF, (hex >> 16) & 0xFF, ((hex >> 8) & 0xFF)> / 255.0;
    }
    
    return <1,1,1>; // default to white if failed
}

default {
    state_entry() {
        list p = [ ];
        
        // face 0: corset main
        p += [ PRIM_COLOR, 0, GetColor(mainColor), 1.0 ];
        p += [ PRIM_TEXTURE, 0, txDiffuse, texReps, texOffset, texRot ];
        p += [ PRIM_NORMAL, 0, txNormal, texReps, texOffset, texRot ];
        p += [ PRIM_SPECULAR, 0, txSpecular, texReps, texOffset, texRot, GetColor(shineColor), mainGloss, mainEnv ];
        p += [ PRIM_ALPHA_MODE, 0, alphaMode, 127 ];
        
        // face 1: lacing
        p += [ PRIM_COLOR, 1, GetColor(lacingColor), 1.0 ];
        p += [ PRIM_TEXTURE, 1, TEXTURE_BLANK, texReps, texOffset, texRot ];
        //p += [ PRIM_NORMAL, 1, NULL_KEY, texReps, texOffset, texRot ];
        p += [ PRIM_SPECULAR, 1, TEXTURE_BLANK, texReps, texOffset, texRot, GetColor(lacingShineColor), lacingGloss, lacingEnv ];
        p += [ PRIM_ALPHA_MODE, 1, 2, 127 ];
        
        // face 2: grommets
        p += [ PRIM_COLOR, 2, GetColor(grommetColor), 1.0 ];
        p += [ PRIM_TEXTURE, 2, TEXTURE_BLANK, texReps, texOffset, texRot ];
        //p += [ PRIM_NORMAL, 2, NULL_KEY, texReps, texOffset, texRot ];
        p += [ PRIM_SPECULAR, 2, TEXTURE_BLANK, texReps, texOffset, texRot, GetColor(grommetShineColor), grommetGloss, grommetEnv ];
        p += [ PRIM_ALPHA_MODE, 2, 2, 127 ];
        
        llSetLinkPrimitiveParamsFast(2, p); // and apply
        llOwnerSay("All set!");
        llRemoveInventory(llGetScriptName()); // clean up
    }
}
