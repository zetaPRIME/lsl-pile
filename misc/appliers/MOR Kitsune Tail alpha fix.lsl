default {
    state_entry() {
        list p = [ ];
        
        p += [ PRIM_ALPHA_MODE, ALL_SIDES, PRIM_ALPHA_MODE_MASK, 100 ];
        p += [ PRIM_ALPHA_MODE, 0, PRIM_ALPHA_MODE_NONE, 100 ];
        
        llSetLinkPrimitiveParamsFast(LINK_SET, p); // and apply
        llOwnerSay("All set!");
        llRemoveInventory(llGetScriptName()); // clean up
    }
}
