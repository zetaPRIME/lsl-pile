//  ===================
// == SETTINGS HEADER ==
//  ===================

float glow = 0.0;

integer cch = 0;
string header = "";

//  ===================
// == + =========== + ==
//  ===================

// [sd] Mod Body/Misc Controller v1.0.5
// https://gitlab.com/zetaPRIME/lsl-pile/-/blob/main/%5Bsd%5D%20modkit%20core/Body%20Controller.lsl

/* Copyright (c) 2017-2025 stellarium designs / Zia Satazaki (kiri.mistwalker)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

integer reglowStep = 0;
DoReglowStep() {
    reglowStep -= 1;
    if (reglowStep <= 0) llSetTimerEvent(0);
    else if (reglowStep <= 1) llSetTimerEvent(2);
    else if (reglowStep <= 2) llSetTimerEvent(1);
    else llSetTimerEvent(0.5);
}
StartReglowStep() { reglowStep = 4; DoReglowStep(); }

ReGlow() {
    list qpar = [ ];
    list tqpar = [ ];
    list par = [ ];
    
    integer np = llGetNumberOfPrims();
    integer ns;
    integer ip; integer is;
    
    for (ip = 1; ip <= np; ip++) {
        ns = llGetLinkNumberOfSides(ip);
        par += [PRIM_LINK_TARGET, ip];
        qpar = [ ]; // ARGH don't be an idiot
        tqpar = [ ];
        for (is = 0; is <= ns; is++) {
            qpar += [PRIM_ALPHA_MODE, is, PRIM_COLOR, is];
            tqpar += [PRIM_TEXTURE, is];
        }
        qpar = llGetLinkPrimitiveParams(ip, qpar);
        tqpar = llGetLinkPrimitiveParams(ip, tqpar);
        for (is = 0; is <= ns; is++) {
            float alpha = llList2Float(qpar, is*4 + 3);
            par += [PRIM_GLOW, is, alpha * glow * (llList2Integer(qpar, is*4+0) == PRIM_ALPHA_MODE_EMISSIVE)];
            
            // pbr hide shim
            list txp = llList2List(tqpar, is*4+1, is*4+3);
            if (llGetListLength(txp) < 3) {
                //llOwnerSay("tex params truncated! link " + (string)ip + " face " + (string)is + "; " + llDumpList2String(txp, " / "));
                txp = [<1,1,0>,<0,0,0>,0.0];
            }
            if (alpha <= 0.001) {
                par += [PRIM_GLTF_BASE_COLOR, is, TEXTURE_TRANSPARENT];
                par += txp;
                par += [<1,1,1>, 0.0, PRIM_GLTF_ALPHA_MODE_MASK, 1.0, FALSE];
            } else {
                par += [PRIM_GLTF_BASE_COLOR, is, ""];
                par += txp;
                if (alpha == 1.0) par += ["", "", "", "", ""];
                else par += ["", alpha, "", "", ""];
            }
        }
    }
    llSetLinkPrimitiveParams(0, par);
}

////////////////////
// Command system //
////////////////////

// from Metapplier
integer MatchPattern(string target, string pattern) {
    string ptype = llGetSubString(pattern, 0, 0);
    if (ptype == "*") { // contains-all; ex.: *word1*word2*word3
        list tk = llParseString2List(pattern, ["*"], []);
        integer i; integer im = llGetListLength(tk);
        for (i = 0; i < im; i++) if (llSubStringIndex(target, llList2String(tk, i)) == -1) return 0;
        return 1; // none missing
    }
    else if (ptype == "_") { // case insensitive/lowercase
        return MatchPattern(llToLower(target), llGetSubString(pattern, 1, -1));
    }
    else if (ptype == "!") { // take a wild guess
        return !MatchPattern(target, llGetSubString(pattern, 1, -1));
    }
    // if no type, match exact
    else return target == pattern;
}

string applyName;
FindApplyName() {
    string n = llGetScriptName();
    applyName = n;
    if (n == "Body Controller") {
        // attempt to determine which body we are
        string obj = llGetObjectName();
        /**/ if (MatchPattern(obj, "_*regalia")) {
            applyName = "Regalia";
        }
    }
}

key mSender;
integer onGlobal = 0;
ProcessCommandQueue(list cmds) {
    integer i = 0; integer ii = llGetListLength(cmds);
    for (i = i; i < ii; i++) { // 1 to skip the command header
        ProcessCommand(llParseString2List(llList2String(cmds, i), ["|"], []));
    }
    //
}

ProcessCommand(list par) {
    string cmd = llList2String(par, 0);
    
    if /**/ (0) { }
    else if (cmd=="apply") llMessageLinked(LINK_SET, 5730, "apply|" + applyName + "|" + llList2String(par, 1), NULL_KEY);
    else if (cmd=="install") {
        llSetRemoteScriptAccessPin((integer)llList2String(par,1));
        llRegionSayTo(mSender, -5730, "install.confirm|" + llGetScriptName());
    }
}

////////////
// States //
////////////

default {
    state_entry() {
        ReGlow();
        
        llListen(282827, "", llGetOwner(), "");
        llListen(-282827, "", NULL_KEY, "");
        if (cch != 0) { // don't listen on channel zero, especially not *twice*
            llListen(cch, "", llGetOwner(), "");
            llListen(-cch, "", NULL_KEY, "");
        }
        
        // if this is a more specific type of controller on the Metapplier side, nuke the misc controller
        string chk = "Misc Controller";
        if (llGetScriptName() != chk && llGetInventoryType(chk) == INVENTORY_SCRIPT) llRemoveInventory(chk);
        
        FindApplyName(); // figure out what applier definition set we're using
    }
    
    timer() {
        DoReglowStep(); // follow stepped repeat where applicable
        ReGlow();
    }
    
    changed(integer change) {
        if (change & CHANGED_COLOR) // color or (more importantly) transparency of something changed
            llSetTimerEvent(0.05);
        if (change & CHANGED_OWNER) llResetScript();
    }
    
    link_message(integer sender, integer num, string cmd, key param) {
        if (num != 282827) return; // listening for commands from the applier
        /**/ if (cmd == "setGlow") {
            glow = (float)((string)param);
            StartReglowStep(); // queue up three reglows to change quickly in low lag, but ensure changes go through
        }
    }
    
    listen(integer channel, string name, key id, string msg) {
        if (llGetOwnerKey(id) != llGetOwner()) return;
        string kt = "avatar"; onGlobal = 1;
        if (llAbs(channel) != 282827) { kt = header; onGlobal = 0; }
        
        list cmds = llParseString2List(llStringTrim(msg, STRING_TRIM), ["||"], []);
        { // encapsulate for scope; only need these variables for a short time, let GC toss them after
            list hs = llParseString2List(llList2String(cmds, 0), ["|"], []);
            if (llList2String(hs, 0) != kt) return;
        }
        
        mSender = id;
        ProcessCommandQueue(cmds);
    }
}
