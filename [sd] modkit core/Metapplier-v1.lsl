string url = "http://zetaprime.gitlab.io/sl-internals/-mtpl/avkits.json";
PostRun() { llResetScript(); }

// "Metapplier" v0.5.5 Installed Variant

string ruleset = "";
string skin = "";

string jsonSkins = ""; // to be cleared after variables have been assembled

list varsK = []; // assembled variables
list varsV = [];
list script = []; // current script commands
integer pc; // program counter
integer cmp = 0; // comparison accumulator

list pq = []; // param queue
list pqs = []; // param queue - selected (to be proliferated into pq on selection change and before apply)
list pqst = []; // param queue - tints (pairs of face+vector)

list unselected = []; // prims that haven't been selected
list prims = [];
list selection = []; // currently matched prims
integer selectionFinalized = 0;

vector texReps = <1, 1, 0>;
vector texOffset = <0, 0, 0>;
float texRot = 0;

vector GetColor(string in) {
    in = llStringTrim(in, STRING_TRIM);
    string fc = llGetSubString(in, 0, 0);
    if (fc == "<") return (vector)in;
    else if (fc == "#") {
        in = llGetSubString(in, 1, -1);
        if (llStringLength(in) == 6) in += "FF";
        integer hex = (integer)("0x"+in);
        return <(hex >> 24) & 0xFF, (hex >> 16) & 0xFF, ((hex >> 8) & 0xFF)> / 255.0;
    }
    
    return <1,1,1>; // default to white if failed
}

string GetValue(string valIn) {
    if (llGetSubString(valIn, 0, 0) == "@") {
        integer find = llListFindList(varsK, [llGetSubString(valIn, 1, -1)]);
        if (find == -1) return "";
        return GetValue(llList2String(varsV, find));
    } else return valIn;
}

float ScaleToRep(float s) {
    float sgn = 1; if (s < 0) sgn = -1;
    float as = llFabs(s);
    if (as < 0.01) return 0.01 * sgn;
    return (1.0 / as) * sgn;
}
vector ScaleReps(float x, float y) { return <ScaleToRep(x), ScaleToRep(y), 0>; }

integer MatchPattern(string target, string pattern) {
    string ptype = llGetSubString(pattern, 0, 0);
    if (ptype == "*") { // contains-all; ex.: *word1*word2*word3
        list tk = llParseString2List(pattern, ["*"], []);
        integer i; integer im = llGetListLength(tk);
        for (i = 0; i < im; i++) if (llSubStringIndex(target, llList2String(tk, i)) == -1) return 0;
        return 1; // none missing
    }
    else if (ptype == "_") { // case insensitive/lowercase
        return MatchPattern(llToLower(target), llGetSubString(pattern, 1, -1));
    }
    else if (ptype == "!") { // take a wild guess
        return !MatchPattern(target, llGetSubString(pattern, 1, -1));
    }
    // if no type, match exact
    else return target == pattern;
}

// copy subqueue for each
ExpandAndFinalize() {
    integer i; integer im = llGetListLength(selection);
    integer it; integer itm = llGetListLength(pqst) / 2;
    integer tq = pqs != [] || pqst != []; // "to queue"
    for (i = 0; i < im; i++) {
        integer cp = llList2Integer(selection, i); // current prim
        if (tq) pq += [PRIM_LINK_TARGET, cp] + pqs;
        for (it = 0; it < itm; it++) { // have to do tinting separately
            integer tf = llList2Integer(pqst, it*2);
            vector tc = llList2Vector(pqst, it*2+1);
            if (tf == ALL_SIDES) {
                for (tf = 0; tf < 8; tf++) { // don't need more than 8 unless you're working with a really weirdly cut prim
                    float alph = llList2Float(llGetLinkPrimitiveParams(cp, [PRIM_COLOR, tf]), 1);
                    pq += [PRIM_COLOR, tf, tc, alph];
                }
            } else {
                float alph = llList2Float(llGetLinkPrimitiveParams(cp, [PRIM_COLOR, tf]), 1);
                pq += [PRIM_COLOR, tf, tc, alph];
            }
        }
        // todo: finalize (remove from "unselected")
        integer cpp = llListFindList(unselected, [cp]);
        if (cpp != -1) unselected = llDeleteSubList(unselected, cpp, cpp);
    }
    pqs = [];
    pqst = [];
}

//
//
//

RunScript() {
    integer pcl = llGetListLength(script);
    for (pc = 0; pc < pcl; pc++) {
        RunCommand(llParseString2List(llList2String(script, pc), ["|"], []));
    }
}

Branch(string label) {
    integer find;
    find = llListFindList(llList2List(script, pc, -1), [label]);
    if (find == -1) find = llListFindList(script, [label]);
    else find += pc; // I'm an idiot :D
    if (find == -1) return;
    pc = find;
}

RunCommand(list par) {
    string cmd = llList2String(par, 0);
    if /**/ (cmd == "match") {
        ExpandAndFinalize();
        string pattern = GetValue(llList2String(par, 1));
        selection = [];
        string ctk; integer i; integer im = llGetListLength(prims);
        if (im <= 0) { // single link
            if (MatchPattern(llGetObjectName(), pattern)) selection += LINK_THIS;
        } else for (i = 0; i < im; i++) {
            ctk = llList2String(prims, i);
            if (MatchPattern(ctk, pattern)) selection += i;
        }
        //llOwnerSay("pattern: " + pattern + ", " + llList2CSV(selection));
    }
    else if (cmd == "+match") { // "or" operator, I suppose
        string pattern = GetValue(llList2String(par, 1));
        string ctk; integer i; integer im = llGetListLength(prims);
        if (im <= 0) { // single link
            if (MatchPattern(llGetObjectName(), pattern)) selection += LINK_THIS;
        } else for (i = 0; i < im; i++) {
            ctk = llList2String(prims, i);
            if (llListFindList(selection, [i]) == -1 && MatchPattern(ctk, pattern)) selection += i;
        }
    }
    else if (cmd == "narrow") { // reduce selection to match another pattern
        string pattern = GetValue(llList2String(par, 1));
        list oldsel = selection;
        selection = [];
        string ctk; integer i; integer im = llGetListLength(oldsel);
        if (im <= 0) { // single link
            if (MatchPattern(llGetObjectName(), pattern)) selection += LINK_THIS;
        } else for (i = 0; i < im; i++) {
            ctk = llList2String(prims, llList2Integer(oldsel, i));
            if (MatchPattern(ctk, pattern)) selection += i;
        }
    }
    else if (cmd == "other") {
        ExpandAndFinalize();
        selection = unselected;
        // actually, this is redundant and prevents some narrowing // unselected = [];
        //llOwnerSay("other, " + llList2CSV(selection));
    }
    else if (cmd == "apply") {
        ExpandAndFinalize();
        //llOwnerSay(llList2CSV(pq));
        llSetLinkPrimitiveParamsFast(LINK_ROOT, pq);
        pq = [];
    }
    else if (cmd == "print") {
        list prt = [">> "];
        integer i; integer im = llGetListLength(par);
        for (i = 1; i < im; i++) prt += GetValue(llList2String(par, i));
        llOwnerSay(llDumpList2String(prt, ""));
    }
    // category: flow control
    else if (cmd == "branch") {
        Branch(GetValue(llList2String(par, 1)));
    }
    else if (cmd == "brancheq") { // branch when equal
        if (cmp == 0) Branch(GetValue(llList2String(par, 1)));
    }
    else if (cmd == "branchne") { // branch when not equal
        if (cmp != 0) Branch(GetValue(llList2String(par, 1)));
    }
    
    else if (cmd == "exit") pc = DEBUG_CHANNEL - 1; // overflow pcl
    
    else if (cmd == "testname") {
        cmp = !MatchPattern(llGetObjectName(), GetValue(llList2String(par, 1)));
    }
    else if (cmd == "+testname") {
        cmp += !MatchPattern(llGetObjectName(), GetValue(llList2String(par, 1)));
    }
    else if (cmd == "*testname") {
        cmp *= !MatchPattern(llGetObjectName(), GetValue(llList2String(par, 1)));
    }
    else if (cmd == "testcreator") {
        cmp = !MatchPattern(llGetCreator(), GetValue(llList2String(par, 1)));
    }
    else if (cmd == "+testcreator") { // wait, isn't this an AND? kind of useless for uuids but eh
        cmp += !MatchPattern(llGetCreator(), GetValue(llList2String(par, 1)));
    }
    else if (cmd == "*testcreator") {
        cmp *= !MatchPattern(llGetCreator(), GetValue(llList2String(par, 1)));
    }
    else if (cmd == "testselection") {
        cmp = llGetListLength(selection) <= 0;
    }
    else if (cmd == "testpattern") { // test par1 by string pattern par2
        cmp = !MatchPattern(GetValue(llList2String(par, 1)), GetValue(llList2String(par, 2)));
    }
    else if (cmd == "+testpattern") { // AND
        cmp += !MatchPattern(GetValue(llList2String(par, 1)), GetValue(llList2String(par, 2)));
    }
    else if (cmd == "*testpattern") { // OR
        cmp *= !MatchPattern(GetValue(llList2String(par, 1)), GetValue(llList2String(par, 2)));
    }
    else if (cmd == "testscript") {
        cmp = llGetInventoryType(GetValue(llList2String(par, 1))) != INVENTORY_SCRIPT;
    }
    else if (cmd == "+testscript") {
        cmp += llGetInventoryType(GetValue(llList2String(par, 1))) != INVENTORY_SCRIPT;
    }
    else if (cmd == "*testscript") {
        cmp *= llGetInventoryType(GetValue(llList2String(par, 1))) != INVENTORY_SCRIPT;
    }
    
    // category: set attributes
    else if (cmd == "resettexattr") {
        texReps = <1, 1, 0>; texOffset = <0, 0, 0>; texRot = 0;
    }
    else if (cmd == "settexattr") { // 1, 2 as scale; 3, 4 as offset; 5 as rotation
        texReps = ScaleReps((float)GetValue(llList2String(par, 1)), (float)GetValue(llList2String(par, 2)));
        texOffset = <(float)GetValue(llList2String(par, 3)), (float)GetValue(llList2String(par, 4)), 0>;
        texRot = (float)GetValue(llList2String(par, 5));
    }
    else if (cmd == "diffuse") {
        pqs += [PRIM_TEXTURE, (integer)GetValue(llList2String(par, 1)), GetValue(llList2String(par, 2)),
            texReps, texOffset, texRot];
    }
    else if (cmd == "normal") {
        pqs += [PRIM_NORMAL, (integer)GetValue(llList2String(par, 1)), GetValue(llList2String(par, 2)),
            texReps, texOffset, texRot];
    }
    else if (cmd == "specular") {
        pqs += [PRIM_SPECULAR, (integer)GetValue(llList2String(par, 1)), GetValue(llList2String(par, 2)),
            texReps, texOffset, texRot, GetColor(GetValue(llList2String(par, 5))), (integer)GetValue(llList2String(par, 3)),
            (integer)GetValue(llList2String(par, 4))];
    }
    else if (cmd == "alpha") {
        pqs += [PRIM_ALPHA_MODE, (integer)GetValue(llList2String(par, 1)), (integer)GetValue(llList2String(par, 2)), 127];
    }
    else if (cmd == "glow") {
        pqs += [PRIM_GLOW, (integer)GetValue(llList2String(par, 1)), (float)GetValue(llList2String(par, 2))];
    }
    else if (cmd == "tint") {
        pqst += [(integer)GetValue(llList2String(par, 1)), GetColor(GetValue(llList2String(par, 2)))];
    }
    // todo: uhh, maybe animation? maybe more flow control, more variable shenanigans
    // match modes etc.?
    
    // category: interop
    else if (cmd == "linkmsg") {
        llMessageLinked((integer)GetValue(llList2String(par, 1)), (integer)GetValue(llList2String(par, 2)),
            GetValue(llList2String(par, 3)), GetValue(llList2String(par, 4)));
    }
    else if (cmd == "attachmsg") {
        llRegionSayTo(llGetOwner(), (integer)GetValue(llList2String(par, 1)), GetValue(llList2String(par, 2)));
    }
    
}

Apply(string _ruleset, string _skin) {
    ruleset = _ruleset; skin = _skin;
    string blah = ruleset; blah = skin; // force-refresh
    string variant = "";
    if (llGetSubString(skin, 0, 0) == ":") variant = llList2String(llParseString2List(skin, ["+"], []), 0);
    llHTTPRequest(url + variant, [HTTP_BODY_MAXLENGTH, 16384, HTTP_CUSTOM_HEADER, "cache-control", "max-age=15"], "");
}

BuildPrimList() {
    // fetch prim list
    prims = []; unselected = [];
    integer np = llGetNumberOfPrims();
    list qp = [];
    integer i = np > 1;
    if (i) { np++; prims = [-1]; }
    for (; i < np; i++) {
        qp += [PRIM_LINK_TARGET, i, PRIM_NAME];
        unselected += i;
    }
    prims += llGetLinkPrimitiveParams(LINK_ROOT, qp); qp = [];
}

ForceGC() { // force garbage collection at any time
    integer lim = llGetMemoryLimit();
    llSetMemoryLimit(lim-1); llSetMemoryLimit(lim);
}

default
{
    state_entry() { }
    
    link_message(integer sender, integer val, string msg, key id) { // 5730: apply|ruleset|skin(s)
        if (val != 5730) return;
        list par = llParseString2List(msg, ["|"], []);
        if (llList2String(par, 0) == "apply") {
            Apply(llList2String(par, 1), llList2String(par, 2));
        }
    }
    
    http_response(key req, integer status, list meta, string body) {
        if (status == 404) llOwnerSay("[Metapplier] Definition not found; possibly incorrect variant string supplied");
        if (status != 200 && status != 203) { PostRun(); return; } // abort on http fail
        
        meta = []; // we don't need this
        integer i; // predeclare because lsl is annoying
        
        jsonSkins = llJsonGetValue(body, ["skins"]);
        body = llJsonGetValue(body, ["rules", ruleset]); // trim
        
        {
            // assemble vars
            script = [];
            list skinDefs = llJson2List(jsonSkins); jsonSkins = "";
            list skinTk = ["default"] + llParseString2List(skin, ["+"], []);
            integer im = llGetListLength(skinTk);
            for (i = 0; i < im; i++) {
                string stk = llList2String(skinTk, i);
                integer find = llListFindList(skinDefs, [stk]);
                if (find != -1) script = llJson2List(llList2String(skinDefs, find+1)) + script; // prepend
            }
            skinDefs = []; skinTk = [];
            varsK = llList2ListStrided(script, 0, -1, 2);
            varsV = llList2ListStrided([""] + script, 1, -1, 2);
            script = [ ];
        } ForceGC();
        
        BuildPrimList();
        ForceGC();
        
        script = llJson2List(body);
        
        state run;
        //RunScript();
        //PostRun();
    }
}

state run {
    state_entry() {
        RunScript();
        PostRun();
    }
}
