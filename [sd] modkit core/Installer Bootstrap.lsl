// [sd] installer bootstrap v1.0.0
// place in any item you want to install the scripts to, then hit the "install" button

////////////////////
// Command system //
////////////////////

key mSender;
ProcessCommandQueue(list cmds) {
    integer i = 0; integer ii = llGetListLength(cmds);
    for (i = i; i < ii; i++) { // 1 to skip the command header
        ProcessCommand(llParseString2List(llList2String(cmds, i), ["|"], []));
    }
    //
}

ProcessCommand(list par) {
    string cmd = llList2String(par, 0);
    
    if /**/ (cmd=="install") {
        llSetRemoteScriptAccessPin((integer)llList2String(par,1));
        llRegionSayTo(mSender, -5730, "install.confirm");
        llRemoveInventory(llGetScriptName());
    }
}

////////////
// States //
////////////

default
{
    state_entry()
    {
        integer ch = 282827;
        llListen(-ch, "", NULL_KEY, "");
    }
    
    changed(integer change) { if (change & CHANGED_OWNER) llResetScript(); }
    
    listen(integer channel, string name, key id, string msg)
    {
        if (llGetOwnerKey(id) != llGetOwner()) return;
        
        list cmds = llParseString2List(llStringTrim(msg, STRING_TRIM), ["||"], []);
        { // encapsulate for scope; only need these variables for a short time, let GC toss them after
            list hs = llParseString2List(llList2String(cmds, 0), ["|"], []);
            if (llList2String(hs, 0) != "avatar") return;
        }
        
        mSender = id;
        ProcessCommandQueue(cmds);
    }
}
