//  ===================
// == SETTINGS HEADER ==
//  ===================

// For mod integration; don't worry about this otherwise!
integer cch = 0;
string header = "";

// How much glow the textures need, if using emissive mask
float glow = 0;

// stock eyelashes: 8c018374-80e5-d419-c475-74e00728fd0f
key lash = "8c018374-80e5-d419-c475-74e00728fd0f";

//  ===================
// == + =========== + ==
//  ===================

// [sd] PAWS Feline Head Controller v1.0.3
// https://gitlab.com/zetaPRIME/lsl-pile/-/blob/main/%5Bsd%5D%20modkit%20core/Head%20Controller.lsl
// May work with other PAWS heads, though no guarantees.
// (Now adjusted to work with female horse head)

/* Copyright (c) 2017-2022 stellarium designs / Zia Satazaki (kiri.mistwalker)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

float untilNextBlink = 0;
integer acBlink = 0;
integer acTalk = 0;

integer ovrEyes = 0;
integer ovrMuzzle = 0;

integer eyelashFace = 1;

integer onGlobal = 0;

key lock = NULL_KEY;

// 

integer OwnerTest(key id) {
    key io = llGetOwnerKey(id);
    if (io == llGetOwner()) return 1; // working as normal
    if (io != id) return 0; // owned by someone/something else and neither an avatar nor gone
    if (llGetAgentSize(id) == ZERO_VECTOR) return 1; // not a present avatar, probably derezzing object
    return 0; // fallthrough, assume false
}

integer LockTest(key id) {
    if (lock == NULL_KEY) return 1; // no lock
    if (llGetOwnerKey(lock) == lock) { // invalid lock
        lock = NULL_KEY;
        return 1;
    }
    if (lock == id) return 1;
    return 0;
}

// muzzle.6 eyelid.5
list lmuzzle = [];
list leye = [];
BuildLists() {
    // also set up other traits here
    string n = llGetLinkName(LINK_ROOT);
    if (llSubStringIndex(n, "Horse") != -1) eyelashFace = 2;
    else eyelashFace = 1;
    
    leye = [99,99,99,99,99,99];
    lmuzzle = leye;
    integer i; integer im = llGetNumberOfPrims();
    for (i = 1; i <= im; i++) {
        string ln = llGetLinkName(i);
        if (llGetSubString(ln, 0, 6) == "eyelid.") { // 0-5
            integer num = (integer)llGetSubString(ln, 7, -1);
            leye = llListReplaceList(leye, [i], num, num);
        }
        else if (llGetSubString(ln, 0, 6) == "muzzle.") { // 1-6
            integer num = (integer)llGetSubString(ln, 7, -1) - 1;
            lmuzzle = llListReplaceList(lmuzzle, [i], num, num);
        }
    }
    
    // fix lashes
    if (lash != NULL_KEY) {
        list pfix = [ ];
        for (i = 0; i <= 5; i++) {
            pfix += [PRIM_LINK_TARGET, llList2Integer(leye, i),
                PRIM_TEXTURE, 1, lash, <1, 1, 0>, <0, 0, 0>, 0,
                PRIM_ALPHA_MODE, 1, PRIM_ALPHA_MODE_BLEND, 0,
                PRIM_SPECULAR, 1, NULL_KEY, <1, 1, 0>, <0, 0, 0>, 0, <1, 1, 1>, 0, 0
            ];
        }
        llSetLinkPrimitiveParamsFast(LINK_ROOT, pfix);
    }
}

SetMuzzle(integer level) {
    if (level < ovrMuzzle) level = ovrMuzzle;
    else if (ovrMuzzle < 0) level = -(ovrMuzzle + 1);
    integer i; list par = [];
    for (i = 0; i <= 5; i++) {
        par += [PRIM_LINK_TARGET, llList2Integer(lmuzzle, i),
            PRIM_COLOR, ALL_SIDES, <1,1,1>, (float)(i == level), PRIM_GLOW, ALL_SIDES, glow * (i == level)];
    }
    llSetLinkPrimitiveParamsFast(LINK_ROOT, par);
}

SetEyes(integer level) {
    if (level < ovrEyes) level = ovrEyes;
    else if (ovrEyes < 0) level = -(ovrEyes + 1);
    integer i; list par = [];
    for (i = 0; i <= 5; i++) {
        par += [PRIM_LINK_TARGET, llList2Integer(leye, i),
            PRIM_COLOR, ALL_SIDES, <1,1,1>, (float)(i == level), PRIM_GLOW, ALL_SIDES, glow * (i == level && llGetLinkNumberOfSides(i) > 1),
            PRIM_GLOW, eyelashFace, 0.0]; // zero glow on eyelashes kthx
    }
    llSetLinkPrimitiveParamsFast(LINK_ROOT, par);
}

SetBlush(float level) {
    llMessageLinked(LINK_SET, -172, "blush|" + (string)level, "");
}

////////////////////
// Command system //
////////////////////

key mSender;
ProcessCommandQueue(list cmds) {
    integer i = 0; integer ii = llGetListLength(cmds);
    for (i = i; i < ii; i++) { // 1 to skip the command header
        ProcessCommand(llParseString2List(llList2String(cmds, i), ["|"], []));
    }
    //
}

ProcessCommand(list par) {
    string cmd = llList2String(par, 0);
    integer num = llGetListLength(par);
    
    if /**/ (cmd=="eyes.state") { ovrEyes = (integer)llList2String(par, 1); SetEyes(0); }
    else if (cmd=="muzzle.state" || cmd=="mouth.state") { 
        if ((num >= 2 && llList2String(par, 1) == "force") || LockTest(mSender))
            { ovrMuzzle = (integer)llList2String(par, 1); SetMuzzle(0); }
    }
    else if (cmd=="blush.state") { SetBlush((float)llList2String(par, 1)); }
    else if (cmd=="lock.set") {
        if ((num >= 2 && llList2String(par, 1) == "force") || LockTest(mSender)) { lock = mSender; }
    }
    else if (cmd=="lock.release") {
        if ((num >= 2 && llList2String(par, 1) == "force") || LockTest(mSender)) { lock = NULL_KEY; }
    }
    else if (0) { }
    //else if (cmd=="apply") llMessageLinked(LINK_SET, 5730, "apply|AIOv1|" + llList2String(par, 1), NULL_KEY);
    else if (cmd=="apply") llMessageLinked(LINK_SET, 5730, "apply|" + llGetScriptName() + "|" + llList2String(par, 1), NULL_KEY);
    else if (cmd=="install") {
        llSetRemoteScriptAccessPin((integer)llList2String(par,1));
        llRegionSayTo(mSender, -5730, "install.confirm|" + llGetScriptName());
    }
}

////////////
// States //
////////////

default
{
    state_entry()
    {
        integer ic = llGetInventoryNumber(INVENTORY_SCRIPT);
        while (ic--) { // set stock scripts inactive
            string name = llGetInventoryName(INVENTORY_SCRIPT, ic);
            if (llSubStringIndex(name, ".:BLK:.") != -1) llSetScriptState(name, 0);
        }
        
        BuildLists();
        SetEyes(0); SetMuzzle(0);
        
        llListen(282827, "", llGetOwner(), "");
        llListen(-282827, "", NULL_KEY, "");
        if (cch != 0) { // don't listen on channel zero, especially not *twice*
            llListen(cch, "", llGetOwner(), "");
            llListen(-cch, "", NULL_KEY, "");
        }
        
        llSetTimerEvent(1.0/20.0);
    }
    
    changed(integer change) { if (change & CHANGED_OWNER) llResetScript(); }
    
    link_message(integer sender, integer num, string cmd, key param) {
        if (num != 282827) return; // listening for commands from the applier
        /**/ if (cmd == "setGlow") {
            glow = (float)((string)param);
            // just set everything to the correct glow level and let animation fix it
            llSetLinkPrimitiveParamsFast(LINK_SET, [PRIM_GLOW, ALL_SIDES, glow]);
            SetEyes(0); SetMuzzle(0); // blip in animation frame is better than sudden multiglow
        }
    }
    
    listen(integer channel, string name, key id, string msg)
    {
        if (channel < 0 && !OwnerTest(id)) return; // not owner or attachment thereof
        string kt = "avatar"; onGlobal = 1;
        if (llAbs(channel) != 282827) { kt = header; onGlobal = 0; }
        
        list cmds = llParseString2List(llStringTrim(msg, STRING_TRIM), ["||"], []);
        { // encapsulate for scope; only need these variables for a short time, let GC toss them after
            list hs = llParseString2List(llList2String(cmds, 0), ["|"], []);
            if (llList2String(hs, 0) != kt) return;
        }
        
        mSender = id;
        ProcessCommandQueue(cmds);
    }
    
    timer()
    {
        untilNextBlink -= llGetAndResetTime();
        if (acBlink < 6) SetEyes(acBlink);
        else SetEyes(5 - (acBlink- 5));
        if (acBlink > 0) acBlink--;
        else if (untilNextBlink <= 0) {
            acBlink = 9;
            untilNextBlink = llFrand(7.0) + 3.0;
        }
        
        if (acTalk < 6) SetMuzzle(acTalk);
        else SetMuzzle(5 - (acTalk - 5));
        if (acTalk > 0) acTalk--;
        else if (llGetAgentInfo(llGetOwner()) & AGENT_TYPING) acTalk = 9;
    }
}
