//  ===================
// == SETTINGS HEADER ==
//  ===================

vector baseSize = <0.5, 0.5, 0.5>;

vector flipVec(vector in) { return <in.x * -1.0, in.y, in.z>; }

integer cch = 0;
string header = "";

//  ===================
// == + =========== + ==
//  ===================

// [sd] Fennec Ears Controller v1.1.3
// https://gitlab.com/zetaPRIME/lsl-pile/-/blob/main/%5Bsd%5D%20modkit%20core/parts/Fennec%20Ears%20Controller.lsl

/* Copyright (c) 2019-2022 stellarium designs / Zia Satazaki (kiri.mistwalker)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

////////////////////
// Pose/Animation //
////////////////////

integer foxMode = 0;

string curPose = "idle";
SetPose(string pose) {
    foxMode = ( llSubStringIndex(llGetObjectDesc(), "+foxMode") > -1 );
    if /**/ (pose == "idle") {
        if (foxMode) SetEarsPosition(20.0, 52.0, 10.0, 0.82, -0.002, 0);
        else SetEarsPosition(10.0, 36.0, 15.0, 1, 0, 0);
    }
    else if (pose == "splay" || pose == "down") {
        if (foxMode) SetEarsPosition(10.0, 12.0, 18.0, 0.8, 0.011, 0);
        else SetEarsPosition(10.0, 12.0, 18.0, 1.1, 0.032, 0);
    }
    else if (pose == "perk") {
        if (foxMode) SetEarsPosition(7.5, 57.0, 0.0, 0.72, -0.005, 0);
        else SetEarsPosition(7.5, 45.0, 0.0, 0.8, -0.002, 0);
    }
    else if (pose == "back") {
        SetEarsPosition(8.0, 32.0, 45.0, 0.875, 0.024, 0.02);
    }
    else if (pose == "pin") {
        SetEarsPosition(20.0, 10.0, 42.0, 0.825, 0.034, 0.031);
    }
    
    else SetPose("idle");
    curPose = pose;
}

float maxDim(vector v) {
    if (v.x > v.y && v.x > v.z) return v.x;
    if (v.y > v.z) return v.y;
    return v.z;
}

SetEarsPosition(float rfwd, float rup, float rback, float outP, float pushdown, float pushback) {
    float sz = 0.75;
    if (foxMode) sz = 0.525;
    vector sc = llGetScale(); // root prim scale
    sz *= maxDim(sc) / 0.05;
    
    vector rootpos = <0.092 * outP, -0.024 + pushback, 0.026 - pushdown>;
    
    rotation rleft = llEuler2Rot(<0, 90, 0> * DEG_TO_RAD);
    rotation rright = llEuler2Rot(<0, -90, 0> * DEG_TO_RAD);
    rleft *= llEuler2Rot(<rfwd, 0, 0> * DEG_TO_RAD);
    rright *= llEuler2Rot(<rfwd, 0, 0> * DEG_TO_RAD);
    rleft *= llEuler2Rot(<0, -rup, 0> * DEG_TO_RAD);
    rright *= llEuler2Rot(<0, rup, 0> * DEG_TO_RAD);
    rleft *= llEuler2Rot(<0, 0, rback> * DEG_TO_RAD);
    rright *= llEuler2Rot(<0, 0, -rback> * DEG_TO_RAD);
    
    llSetLinkPrimitiveParamsFast(0, [
        PRIM_LINK_TARGET, 2,
        PRIM_SIZE, baseSize * sz,
        PRIM_ROT_LOCAL, rleft,
        PRIM_POS_LOCAL, rootpos,
        PRIM_LINK_TARGET, 3,
        PRIM_SIZE, baseSize * sz,
        PRIM_ROT_LOCAL, rright,
        PRIM_POS_LOCAL, flipVec(rootpos)
    ]);
}

////////////////////
// Command system //
////////////////////

key mSender;
integer onGlobal = 0;
ProcessCommandQueue(list cmds) {
    integer i = 0; integer ii = llGetListLength(cmds);
    for (i = i; i < ii; i++) { // 1 to skip the command header
        ProcessCommand(llParseString2List(llList2String(cmds, i), ["|"], []));
    }
    //
}

ProcessCommand(list par) {
    string cmd = llList2String(par, 0);
    
    if /**/ (cmd=="ears.pose") { SetPose(llList2String(par, 1)); }
    else if (cmd=="apply") llMessageLinked(LINK_SET, 5730, "apply|" + llGetScriptName() + "|" + llList2String(par, 1), NULL_KEY);
    else if (cmd=="install") {
        llSetRemoteScriptAccessPin((integer)llList2String(par,1));
        llRegionSayTo(mSender, -5730, "install.confirm|" + llGetScriptName());
    }
}

////////////
// States //
////////////

default
{
    state_entry()
    {
        SetPose("invalid pose so it takes the default");
        llListen(282827, "", llGetOwner(), "");
        llListen(-282827, "", NULL_KEY, "");
        if (cch != 0) { // don't listen on channel zero, especially not *twice*
            llListen(cch, "", llGetOwner(), "");
            llListen(-cch, "", NULL_KEY, "");
        }
    }
    
    on_rez(integer sp) { SetPose(curPose); }
    
    changed(integer change) {
        if (change & CHANGED_OWNER) llResetScript();
        if (change & CHANGED_SCALE) SetPose(curPose);
    }
    
    listen(integer channel, string name, key id, string msg)
    {
        if (llGetOwnerKey(id) != llGetOwner()) return;
        string kt = "avatar"; onGlobal = 1;
        if (llAbs(channel) != 282827) { kt = header; onGlobal = 0; }
        
        list cmds = llParseString2List(llStringTrim(msg, STRING_TRIM), ["||"], []);
        { // encapsulate for scope; only need these variables for a short time, let GC toss them after
            list hs = llParseString2List(llList2String(cmds, 0), ["|"], []);
            if (llList2String(hs, 0) != kt) return;
        }
        
        mSender = id;
        ProcessCommandQueue(cmds);
    }
}
