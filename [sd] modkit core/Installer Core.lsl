//  ===================
// == SETTINGS HEADER ==
//  ===================

integer cch = 0;
string header = "";
string defaultSkin = "";

list DefaultInstalls(key id, string name) {
    key creator = llList2Key(llGetObjectDetails(id, [OBJECT_CREATOR]), 0);
    string lname = llToLower(name);
    
    if (creator == "20e49fd6-47f5-4582-bf5f-b912e093895c") { // PAWS heads
        return ["Head Controller"];
    }
    else if (creator == "e07f21dd-b22c-4d79-bf51-cf05499f5cd0") { // mod items
        //if (llSubStringIndex(lname, "ears") != -1) return ["Ears Controller"];
        //else if (llSubStringIndex(lname, "tail") != -1) return ["Tail Controller"];
    }
    else if (llSubStringIndex(lname, "regalia") != -1) return ["Body Controller"]; // Regalia body + enhancers
    else if (llSubStringIndex(lname, "kemono") != -1 || KemoMatch(name)) return ["Body Controller"]; // kemono enhancers
    
    return ["Misc Controller"];
}

//  ===================
// == + =========== + ==
//  ===================

// [sd] Mod Installer Core v1.0.2
// https://gitlab.com/zetaPRIME/lsl-pile/-/blob/main/%5Bsd%5D%20modkit%20core/Installer%20Core.lsl

/* Copyright (c) 2017-2022 stellarium designs / Zia Satazaki (kiri.mistwalker)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

integer PIN = 21327;

string enhancersUrl = "http://zetaprime.gitlab.io/sl-internals/enhancers.json";
list kemonoEnhancers = [ ];

integer KemoMatch(string name) {
    integer i; integer im = llGetListLength(kemonoEnhancers);
    for (i = 0; i < im; i++) { if (MatchPattern(name, llList2String(kemonoEnhancers, i))) return 1; }
    return 0;
}

integer MatchPattern(string target, string pattern) {
    string ptype = llGetSubString(pattern, 0, 0);
    if (ptype == "*") { // contains-all; ex.: *word1*word2*word3
        list tk = llParseString2List(pattern, ["*"], []);
        integer i; integer im = llGetListLength(tk);
        for (i = 0; i < im; i++) if (llSubStringIndex(target, llList2String(tk, i)) == -1) return 0;
        return 1; // none missing
    }
    else if (ptype == "_") { // case insensitive/lowercase
        return MatchPattern(llToLower(target), llGetSubString(pattern, 1, -1));
    }
    else if (ptype == "!") { // take a wild guess
        return !MatchPattern(target, llGetSubString(pattern, 1, -1));
    }
    // if no type, match exact
    else return target == pattern;
}

string instName; // original name
integer installing = 0;
integer dispatchCount = 0;
integer curDispatch = 0;
list taken = [ ];

Install(key id, string script, integer pin) {
    if (script == "Tail Controller") SendAnimations(id, "tailpose:"); // send tail animations with tail controller, if present
    llMessageLinked(LINK_SET, 2210,
        "install|" + (string)curDispatch + "|" + (string)id + "|" + script + "|" + (string)pin + "|0", "");
    
    curDispatch++;
    if (curDispatch >= dispatchCount) {
        curDispatch = 0;
        llSleep(3.1); // stay synced
    }
}

SendAnimations(key id, string pfx) {
    list lst = [];
    integer ic = llGetInventoryNumber(INVENTORY_ANIMATION);
    while (ic--) {
        string name = llGetInventoryName(INVENTORY_ANIMATION, ic);
        if (llSubStringIndex(name, pfx) == 0) {
            lst += [name];
        }
    }
    
    integer ch = 282827; string pfx = "avatar";
    if (cch != 0) { ch = cch; pfx = header; }
    llRegionSayTo(id, -ch, pfx + "||pruneAnimations|" + llDumpList2String(lst, "|"));
    llSleep(0.5); // let the prune go through before moving on
    
    ic = llGetListLength(lst);
    while (ic--) { // and give the item
        llGiveInventory(id, llList2String(lst, ic));
    }
}

// handle full install one item at a time
InstallTo(key id, string name, string script) {
    if (llListFindList(taken, [id]) != -1) return; // already handled
    taken = taken + [id]; // else add to tracker
    llSetTimerEvent(0); // cancel premature "completion"
    
    Install(id, "Metapplier (Installed)", PIN); // always install latest applier
    if (script != "") {
        if (llGetInventoryType(script) == INVENTORY_SCRIPT) Install(id, script, PIN); // update existing controller
    } else {
        list l = DefaultInstalls(id, name);
        integer i;
        for (i = 0; i < llGetListLength(l); i++) Install(id, llList2String(l, i), PIN);
    }
    
    llOwnerSay("Finished install to: " + name);
    llSetTimerEvent(5);
}

default
{
    state_entry()
    {
        integer ic = llGetInventoryNumber(INVENTORY_SCRIPT);
        while (ic--) { // fetch dispatch count
            string name = llGetInventoryName(INVENTORY_SCRIPT, ic);
            if (llSubStringIndex(name, "~dispatch") != -1) dispatchCount++;
        }
        
        llListen(-5730, "", NULL_KEY, "");
        llHTTPRequest(enhancersUrl, [HTTP_BODY_MAXLENGTH, 16384, HTTP_CUSTOM_HEADER, "cache-control", "max-age=15"], "");
    }
    
    attach(key id)
    {
        if (id == NULL_KEY) return;
        llHTTPRequest(enhancersUrl, [HTTP_BODY_MAXLENGTH, 16384, HTTP_CUSTOM_HEADER, "cache-control", "max-age=15"], "");
    }
    
    http_response(key req, integer status, list meta, string body) {
        if (status != 200 && status != 203) { return; } // abort on http fail
        kemonoEnhancers = llJson2List(llJsonGetValue(body, ["kemono"]));
    }
    
    listen(integer channel, string name, key id, string msg)
    {
        if (llGetOwnerKey(id) != llGetOwner()) return;
        list lst = llParseString2List(msg, ["|"], [ ]);
        if (llList2String(lst, 0) == "install.confirm") InstallTo(id, name, llList2String(lst, 1));
    }

    touch_start(integer total_number)
    {
        if (installing != 0) return;
        installing = 1; instName = llGetObjectName();
        curDispatch = 0; taken = [ ];
        llSetObjectName("Installer");
        llOwnerSay("Beginning installation...");
        llRegionSayTo(llGetOwner(), -282827, "avatar||install|" + (string)PIN);
        if (cch != 0) llRegionSayTo(llGetOwner(), -cch, header + "||install|" + (string)PIN);
    }
    
    timer()
    {
        llSetTimerEvent(0); // stop
        llOwnerSay("Installation finished!");
        installing = 0;
        llSetObjectName(instName);
        
        if (defaultSkin == "") llMessageLinked(LINK_ROOT, 1337, "APPLY", ""); // trigger main HUD to apply selection
        else llRegionSayTo(llGetOwner(), -282827, "avatar||apply|" + defaultSkin);
    }
}
