// [sd] retalker v0.004
// https://gitlab.com/zetaPRIME/lsl-pile/-/blob/main/%5Bsd%5D%20retalker.lsl

string talkerName;

vector textColor = <1.0, 1.0, 1.0>;

string objName;
integer rtChannel = 39071; // whatever
integer toyChannel = 3571;
integer pokeChannel = 133794022;
integer isHud;
integer isTitler;
integer isSqueaker;

sayPub(string msg) {
    llSetObjectName(talkerName);
    llSay(0, msg);
    llSetObjectName(objName);
}

sayPriv(string name, string msg) {
    llSetObjectName(name);
    llRegionSayTo(llGetOwner(), 0, msg);
    llSetObjectName(objName);
}

default {
    state_entry() {
        integer disable = 0;
        
        integer attachPt = llGetAttached();
        isHud = attachPt >= 31 && attachPt <= 38;
        
        { // set up name stuff
            objName = llGetObjectName();
            list objTk = llParseString2List(objName, ["//"], []);
            if (talkerName == "") talkerName = llStringTrim(llList2String(objTk, 1), STRING_TRIM);
            isTitler = llSubStringIndex(llToLower(llList2String(objTk, 0)), "titler") != -1;
            isSqueaker = llSubStringIndex(llToLower(llList2String(objTk, 0)), "squeaker") != -1;
        }
        
        if (talkerName == llGetDisplayName(llGetOwner())) { // disable when display name is same
            llSetText("", textColor, 0.0);
            // still act as toy translator
            if (!isTitler) llListen(toyChannel, "", NULL_KEY, "");
            return;
        }
        
        if (isHud) {
            llSetText("", textColor, 0.0);
        } else {
            textColor = llGetColor(0);
            llSetText("[ " + talkerName + " ]", textColor, 1.0);
        }
        
        if (!isTitler) {
            if (isSqueaker) {
                llListen(0, "", llGetOwner(), "");
            } else {
                // todo: maybe randomize rtChannel
                llListen(rtChannel, "", llGetOwner(), "");
                llOwnerSay("@clear=redir,redirchat:" + (string)rtChannel + "=add,rediremote:" + (string)rtChannel + "=add");
            }
            
            // also serve as translator for certain other retalker types
            llListen(toyChannel, "", NULL_KEY, "");
            llListen(pokeChannel, "", NULL_KEY, "");
        }
        
    }
    
    on_rez(integer params) { llResetScript(); }

    listen(integer channel, string name, key id, string msg) {
        if ((channel == rtChannel || channel == 0) && id == llGetOwner()) {
            if (channel != 0) sayPub(msg); // retalk
            if (llToLower(llGetSubString(msg, 0, 3)) != "/me ") { // not emote, do sound
                integer numSnd = llGetInventoryNumber(INVENTORY_SOUND);
                if (numSnd > 0) llTriggerSound(llGetInventoryName(INVENTORY_SOUND, (integer)llFrand(numSnd)), 1.0);
            }
        } else if (channel == toyChannel && llGetSubString(msg, 0, 1) == "§¦") {
            string mmsg = llDeleteSubString(msg, 0, 1);
            sayPriv(name, "(toy squeaks) " + mmsg);
        } else if (channel == pokeChannel) {
            sayPriv(name, "(pokéspeak) " + msg);
        }
    }
}
