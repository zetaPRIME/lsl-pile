/*
    [sd] rescript for <R-D> chastity cages, v1.0.2
    https://gitlab.com/zetaPRIME/lsl-pile/-/blob/main/rd-cage-controller.lsl
    built on OpenCollar's addon template
    
    To use: simply create a new script, paste this file in, and drop it into the contents tab of the cage in build mode.
    !! MAKE A BACKUP FIRST !!
    This will delete any old OpenCollar scripts automatically!
*/

// // //

integer lnCage;
integer lnLock;
integer lnShockBox; integer lnShockLED;
integer lnSound;

vector getLinkColor(integer n) {
    return llList2Vector(llGetLinkPrimitiveParams(n, [PRIM_COLOR, 0]), 0);
}

integer bVisible = TRUE;
integer bLocked = FALSE;
integer bSound = FALSE;
integer bShockBox = FALSE;

integer bAllowHide = FALSE;
integer bAllowHideSet = FALSE;

list _pp = [ ];
integer _target = 0;
setTarget(integer ln) { _target = ln; _pp += [PRIM_LINK_TARGET, ln]; }
setAlpha(float a) { _pp += [PRIM_COLOR, ALL_SIDES, getLinkColor(_target), a]; }

setVisible(integer b) {
    bVisible = b;
    Link("from_addon", LM_SETTING_SAVE, "rdcage_visible="+(string)b, "");
    refreshVis();
}

setLocked(integer b) {
    bLocked = b;
    if (b) llOwnerSay("@detach=n");
    else llOwnerSay("@detach=y");
    
    refreshVis(); // I think we always want to do this here?
}

setSound(integer b) {
    bSound = b;
    Link("from_addon", LM_SETTING_SAVE, "rdcage_sound="+(string)b, "");
    if (bVisible) refreshVis();
}

setShockBox(integer b) {
    bShockBox = b;
    Link("from_addon", LM_SETTING_SAVE, "rdcage_shockbox="+(string)b, "");
    if (bVisible) refreshVis();
}

setAllowHide(integer b) {
    bAllowHide = b;
    Link("from_addon", LM_SETTING_SAVE, "rdcage_allowhide="+(string)b, "");
}

refreshVis() {
    _pp = [ ];
    
    setTarget(lnCage); setAlpha(bVisible);
    if (bVisible) { // handle texture
        string tn = "noshockbox";
        if (bShockBox) tn = "shockbox";
        if (llGetInventoryType(tn) == INVENTORY_TEXTURE) {
            _pp += [PRIM_TEXTURE, ALL_SIDES, tn];
            _pp += llList2List(llGetLinkPrimitiveParams(lnCage, [PRIM_TEXTURE, 0]), 1, -1);
        }
    }
    setTarget(lnLock); setAlpha(bVisible && bLocked);
    setTarget(lnSound); setAlpha(bVisible && bSound);
    integer sb = bVisible && bShockBox;
    setTarget(lnShockBox); setAlpha(sb);
    setTarget(lnShockLED); setAlpha(sb); // don't need to set glow here, there's a script in the LED
    
    llSetLinkPrimitiveParamsFast(LINK_SET, _pp);
}

// // //

integer API_CHANNEL = 0x60b97b5e;

//list g_lCollars;
string g_sAddon = "Cage";

//integer CMD_ZERO            = 0;
integer CMD_OWNER           = 500;
//integer CMD_TRUSTED         = 501;
//integer CMD_GROUP           = 502;
integer CMD_WEARER          = 503;
integer CMD_EVERYONE        = 504;
//integer CMD_BLOCKED         = 598; // <--- Used in auth_request, will not return on a CMD_ZERO
//integer CMD_RLV_RELAY       = 507;
//integer CMD_SAFEWORD        = 510;
//integer CMD_RELAY_SAFEWORD  = 511;
//integer CMD_NOACCESS        = 599;

integer LM_SETTING_SAVE     = 2000; //scripts send messages on this channel to have settings saved, <string> must be in form of "token=value"
integer LM_SETTING_REQUEST  = 2001; //when startup, scripts send requests for settings on this channel
integer LM_SETTING_RESPONSE = 2002; //the settings script sends responses on this channel
//integer LM_SETTING_DELETE   = 2003; //delete token from settings
integer LM_SETTING_EMPTY    = 2004; //sent when a token has no value

integer DIALOG          = -9000;
integer DIALOG_RESPONSE = -9001;
integer DIALOG_TIMEOUT  = -9002;

/*
 * Since Release Candidate 1, Addons will not receive all link messages without prior opt-in.
 * To opt in, add the needed link messages to g_lOptedLM = [], they'll be transmitted on
 * the initial registration and can be updated at any time by sending a packet of type `update`
 * Following LMs require opt-in:
 * [ALIVE, READY, STARTUP, CMD_ZERO, MENUNAME_REQUEST, MENUNAME_RESPONSE, MENUNAME_REMOVE, SAY, NOTIFY, DIALOG, SENSORDIALOG]
 */
list g_lOptedLM     = [];

list g_lMenuIDs;
integer g_iMenuStride;

string UPMENU = "BACK";

Dialog(key kID, string sPrompt, list lChoices, list lUtilityButtons, integer iPage, integer iAuth, string sName) {
    key kMenuID = llGenerateKey();
    
    llRegionSayTo(g_kCollar, API_CHANNEL, llList2Json(JSON_OBJECT, [ "pkt_type", "from_addon", "addon_name", g_sAddon, "iNum", DIALOG, "sMsg", (string)kID + "|" + sPrompt + "|" + (string)iPage + "|" + llDumpList2String(lChoices, "`") + "|" + llDumpList2String(lUtilityButtons, "`") + "|" + (string)iAuth, "kID", kMenuID ]));

    integer iIndex = llListFindList(g_lMenuIDs, [kID]);
    if (~iIndex) g_lMenuIDs = llListReplaceList(g_lMenuIDs, [ kID, kMenuID, sName ], iIndex, iIndex + g_iMenuStride - 1);
    else g_lMenuIDs += [kID, kMenuID, sName];
}

list checkboxChars = ["□", "▣"];
integer bool(integer b) { if (b) return TRUE; return FALSE; }
string checkBox(integer b, string label) { return llList2String(checkboxChars, bool(b)) + " " + label; }

integer auth(integer ia) {
    if (bLocked && ia == CMD_WEARER) return FALSE;
    return ia <= CMD_EVERYONE;
}

list gOpt = [UPMENU];

MenuMain(key kID, integer iAuth) {
    string txt = "\n<R-D> Chastity Cage";
    
    list opt = [ ];
    
    integer bAuth = auth(iAuth);
    
    if (bAllowHide || bAuth) opt += [
        checkBox(bVisible, "Visible")
    ];
    
    if (bAuth) opt += [
        checkBox(bSound, "Sound"),
        checkBox(bShockBox, "Shock Box")
    ];
    
    if (iAuth == CMD_OWNER) opt += [
        checkBox(bAllowHide, "Allow Hide")
    ];
    
    Dialog(kID, txt, opt, gOpt, 0, iAuth, "Menu~Main");
}

UserCommand(integer iNum, string sStr, key kID) {
    if (iNum<CMD_OWNER || iNum>CMD_WEARER) return;
    if (llSubStringIndex(llToLower(sStr), llToLower(g_sAddon)) && llToLower(sStr) != "menu " + llToLower(g_sAddon)) return;
    if (iNum == CMD_OWNER && llToLower(sStr) == "runaway") {
        return;
    }
    
    integer bAuth = auth(iNum);
    string cmd = llToLower(sStr);
    if (cmd == llToLower(g_sAddon) || cmd == "menu "+llToLower(g_sAddon)) MenuMain(kID, iNum);
    else if ((cmd == "cage show" || cmd == "cage on" ) && (bAuth || bAllowHide)) setVisible(TRUE);
    else if ((cmd == "cage hide" || cmd == "cage off") && (bAuth || bAllowHide)) setVisible(FALSE);
    else if (auth(iNum)) {
        /**/ if (cmd == "cage sound on") setSound(TRUE);
        else if (cmd == "cage sound off") setSound(FALSE);
        else if (cmd == "cage sound") setSound(!bSound);
        else if (cmd == "cage shockbox on") setShockBox(TRUE);
        else if (cmd == "cage shockbox off") setShockBox(FALSE);
        else if (cmd == "cage shockbox") setShockBox(!bShockBox);
    }
}

Link(string packet, integer iNum, string sStr, key kID) {
    list packet_data = [ "pkt_type", packet, "iNum", iNum, "addon_name", g_sAddon, "bridge", FALSE, "sMsg", sStr, "kID", kID ];

    if (packet == "online" || packet == "update") { // only add optin if packet type is online or update
        llListInsertList(packet_data, [ "optin", llDumpList2String(g_lOptedLM, "~") ], -1);
    }

    string pkt = llList2Json(JSON_OBJECT, packet_data);
    if (g_kCollar != "" && g_kCollar != NULL_KEY) {
        llRegionSayTo(g_kCollar, API_CHANNEL, pkt);
    } else {
        llRegionSay(API_CHANNEL, pkt);
    }
}

key g_kCollar = NULL_KEY;
integer g_iLMLastRecv;
integer g_iLMLastSent;

default {
    state_entry() { // one-time setup
        integer i = 0;
        integer c = llGetInventoryNumber(INVENTORY_SCRIPT);
        list sl = [ ];
        for (i = 0; i < c; i++) {
            string n = llGetInventoryName(INVENTORY_SCRIPT, i);
            if (llSubStringIndex(n, "OpenCollar - ") == 0) sl += [n];
        }
        c = llGetListLength(sl);
        for (i = 0; i < c; i++) llRemoveInventory(llList2String(sl, i)); // remove old OC scripts
        
        state main;
    }
}

state main {
    state_entry() {
        // init addon
        API_CHANNEL = ((integer)("0x" + llGetSubString((string)llGetOwner(), 0, 8))) + 0xf6eb - 0xd2;
        llListen(API_CHANNEL, "", "", "");
        Link("online", 0, "", llGetOwner()); // This is the signal to initiate communication between the addon and the collar
        g_iLMLastRecv = llGetUnixTime(); // need to initialize this here in order to prevent resetting before we can receive our first pong
        llSetTimerEvent(60);
        
        // find components
        integer ps = llGetNumberOfPrims();
        integer i = 0;
        for (i = 1; i <= ps; i++) {
            string lname = llGetLinkName(i);
            /**/ if (lname == "Cage") lnCage = i;
            else if (lname == "Lock") lnLock = i;
            else if (lname == "ShockBox") lnShockBox = i;
            else if (lname == "LED") lnShockLED = i;
            else if (lname == "SoundingRod") lnSound = i;
        }
        
        refreshVis();
    }
    
    attach(key kID){
        if(kID != NULL_KEY) state softReset; // soft reset on attach (force handshake)
        else if (g_kCollar != NULL_KEY) { // notify on detach
            Link("offline", 0, "", llGetOwnerKey(g_kCollar));
            g_lMenuIDs = [];
            g_kCollar = NULL_KEY;
        }
    }
    
    timer() {
        if (llGetUnixTime() >= (g_iLMLastSent + 30)) {
            g_iLMLastSent = llGetUnixTime();
            Link("ping", 0, "", g_kCollar);
        }

        if (llGetUnixTime() > (g_iLMLastRecv + (5 * 60)) && g_kCollar != NULL_KEY) {
            state softReset;
        }
        
        if (llGetTime() > 10 && g_kCollar == NULL_KEY) state softReset;
    }
    
    touch_start(integer num) {
        Link("from_addon", 0, "menu "+g_sAddon, llDetectedKey(0));
    }
    
    listen(integer channel, string name, key id, string msg) {
        string sPacketType = llJsonGetValue(msg, ["pkt_type"]);
        if (sPacketType == "approved" && g_kCollar == NULL_KEY) {
            // This signal, indicates the collar has approved the addon and that communication requests will be responded to if the requests are valid collar LMs.
            g_kCollar = id;
            g_iLMLastRecv = llGetUnixTime(); // initial message should also count as a pong
            Link("from_addon", LM_SETTING_REQUEST, "ALL", "");
        } else if (sPacketType == "dc" && g_kCollar == id) {
            state softReset;
        } else if (sPacketType == "pong" && g_kCollar == id) {
            g_iLMLastRecv = llGetUnixTime();
        } else if(sPacketType == "from_collar") {
            // process link message if in range of addon
            if (llVecDist(llGetPos(), llList2Vector(llGetObjectDetails(id, [OBJECT_POS]), 0)) <= 10.0) {
                integer iNum = (integer) llJsonGetValue(msg, ["iNum"]);
                string sStr  = llJsonGetValue(msg, ["sMsg"]);
                key kID      = (key) llJsonGetValue(msg, ["kID"]);
                
                if (iNum == LM_SETTING_RESPONSE || iNum == LM_SETTING_EMPTY) {
                    list lPar     = llParseString2List(sStr, ["_","="], []);
                    string sToken = llList2String(lPar, 0);
                    string sVar   = llList2String(lPar, 1);
                    string sVal   = llList2String(lPar, 2);
                    
                    if (sToken == "auth") {
                        if (sVar == "owner") {
                            //llSay(0, "owner values is: " + sVal);
                        }
                    } else if (sToken == "global") {
                        if (sVar == "locked") {
                            setLocked((integer)sVal);
                        }
                    } else if (sToken == "rdcage") {
                        if (iNum == LM_SETTING_EMPTY) {
                            //
                        } else {
                            /**/ if (sVar == "visible") bVisible = (integer)sVal;
                            else if (sVar == "sound") bSound = (integer)sVal;
                            else if (sVar == "shockbox") bShockBox = (integer)sVal;
                            else if (sVar == "allowhide") { bAllowHide = (integer)sVal; bAllowHideSet = TRUE; }
                        }
                    }
                    
                    if (sStr == "settings=sent") {
                        if (!bAllowHideSet) { bAllowHide = TRUE; bAllowHideSet = TRUE; } // set default here for security
                        refreshVis(); // commit at end since the only place these *should* be changing is in this script
                    }
                } else if (iNum >= CMD_OWNER && iNum <= CMD_EVERYONE) {
                    UserCommand(iNum, sStr, kID);
                    
                } else if (iNum == DIALOG_TIMEOUT) {
                    integer iMenuIndex = llListFindList(g_lMenuIDs, [kID]);
                    g_lMenuIDs = llDeleteSubList(g_lMenuIDs, iMenuIndex - 1, iMenuIndex + 3);  //remove stride from g_lMenuIDs
                } else if (iNum == DIALOG_RESPONSE) {
                    integer iMenuIndex = llListFindList(g_lMenuIDs, [kID]);
                    if (iMenuIndex != -1) {
                        string sMenu = llList2String(g_lMenuIDs, iMenuIndex + 1);
                        g_lMenuIDs = llDeleteSubList(g_lMenuIDs, iMenuIndex - 1, iMenuIndex - 2 + g_iMenuStride);
                        list lMenuParams = llParseString2List(sStr, ["|"], []);
                        key kAv = llList2Key(lMenuParams, 0);
                        string sMsg = llList2String(lMenuParams, 1);
                        integer iAuth = llList2Integer(lMenuParams, 3);
                        
                        if (sMenu == "Menu~Main") {
                            if (sMsg == UPMENU) {
                                Link("from_addon", iAuth, "menu Addons", kAv);
                            } else if (sMsg == checkBox(bVisible, "Visible")) {
                                setVisible(!bVisible);
                                MenuMain(kAv, iAuth);
                            } else if (sMsg == checkBox(bSound, "Sound")) {
                                setSound(!bSound);
                                MenuMain(kAv, iAuth);
                            } else if (sMsg == checkBox(bShockBox, "Shock Box")) {
                                setShockBox(!bShockBox);
                                MenuMain(kAv, iAuth);
                            } else if (sMsg == checkBox(bAllowHide, "Allow Hide")) {
                                setAllowHide(!bAllowHide);
                                MenuMain(kAv, iAuth);
                            } else if (sMsg == "DISCONNECT") {
                                Link("offline", 0, "", llGetOwnerKey(g_kCollar));
                                g_lMenuIDs = [];
                                g_kCollar = NULL_KEY;
                            }
                        }
                        
                        
                        //
                    }
                }
            }
        }
    }
}

state softReset {
    state_entry() {
        g_lMenuIDs = [ ];
        g_kCollar = NULL_KEY;
        bAllowHideSet = FALSE;
        state main;
    }
}
